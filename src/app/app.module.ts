// Angular dependencies
import {
  BrowserModule
} from '@angular/platform-browser';
import {
  NgModule,
} from '@angular/core';

import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';

import {
  AppRoutingModule
} from './app-routing.module';

import {
  AppComponent
} from './app.component';

import {
  HttpClientModule
} from '@angular/common/http';

// Core modules dependencies
import {
  NavBarComponent
} from './app-core/navbar/navbar.component';

import {
  PageNotFoundComponent
} from './app-core/pagenotfound/pagenotfound.component';

// Auth modules dependencies
import {
  SignInComponent
} from './app-core/auth/signin/signin.component';

// Modules components dependencies
import {
  DashboardComponent
} from './modules/dashboard/dashboard.component';

import {
  VaultsListComponent
} from './modules/vaults/list/vaults-list.component';

import {
  VaultDetailsComponent
} from './modules/vaults/details/vault-details.component';

import {
  BackupsListComponent
} from './modules/backups/list/backups-list.component';

import {
  BackupDetailsComponent
} from './modules/backups/details/backup-details.component';

import {
  SettingsComponent
} from './modules/settings/settings.component';

// Angular material dependencies
import {
  MatToolbarModule,
} from '@angular/material/toolbar';

import {
  MatMenuModule,
} from '@angular/material/menu';

import {
  MatFormFieldModule
} from '@angular/material/form-field';

import {
  MatButtonModule
} from '@angular/material/button';

import {
  MatIconModule
} from '@angular/material/icon';

import {
  MatCardModule
} from '@angular/material/card';

import {
  MatListModule
} from '@angular/material/list';

import {
  MatDividerModule
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,

    // Core components
    NavBarComponent,
    PageNotFoundComponent,

    // Auth components
    SignInComponent,

    // Modules components
    DashboardComponent,
    VaultsListComponent,
    VaultDetailsComponent,
    BackupsListComponent,
    BackupDetailsComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // Angular material
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatCardModule,
    MatListModule,
    MatDividerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
