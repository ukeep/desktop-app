import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {
  HttpClientModule
} from '@angular/common/http';

import { AppComponent } from './app.component';

// Core modules dependencies
import {
  NavBarComponent
} from './app-core/navbar/navbar.component';

import {
  PageNotFoundComponent
} from './app-core/pagenotfound/pagenotfound.component';

// Auth modules dependencies
import {
  SignInComponent
} from './app-core/auth/signin/signin.component';

import {
  LogoutComponent
} from './app-core/auth/logout/logout.component';

import {
  SignUpComponent
} from './app-core/auth/signup/signup.component';

// Modules components dependencies
import {
  DashboardComponent
} from './modules/dashboard/dashboard.component';

import {
  VaultsListComponent
} from './modules/vaults/list/vaults-list.component';

import {
  VaultDetailsComponent
} from './modules/vaults/details/vault-details.component';

import {
  BackupsListComponent
} from './modules/backups/list/backups-list.component';

import {
  BackupDetailsComponent
} from './modules/backups/details/backup-details.component';

import {
  SettingsComponent
} from './modules/settings/settings.component';

// Angular material dependencies
import {
  MatToolbarModule,
} from '@angular/material/toolbar';

import {
  MatMenuModule,
} from '@angular/material/menu';

import {
  MatFormFieldModule
} from '@angular/material/form-field';

import {
  MatButtonModule
} from '@angular/material/button';

import {
  MatIconModule
} from '@angular/material/icon';

import {
  MatCardModule
} from '@angular/material/card';

import {
  MatListModule
} from '@angular/material/list';

import {
  MatDividerModule
} from '@angular/material';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,

        // Angular material
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatCardModule,
        MatListModule,
        MatDividerModule,
      ],
      declarations: [
        AppComponent,

        // Core components
        NavBarComponent,
        PageNotFoundComponent,

        // Auth components
        LogoutComponent,
        SignInComponent,
        SignUpComponent,

        // Modules components
        DashboardComponent,
        VaultsListComponent,
        VaultDetailsComponent,
        BackupsListComponent,
        BackupDetailsComponent,
        SettingsComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
