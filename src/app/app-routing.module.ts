// Angular dependencies
import {
  NgModule,
} from '@angular/core';

import {
  Routes,
  RouterModule,
} from '@angular/router';

// Core dependencies
import {
  PageNotFoundComponent,
} from './app-core/pagenotfound/pagenotfound.component';

// Modules component dependencies
import {
  DashboardComponent,
} from './modules/dashboard/dashboard.component';

import {
  VaultsListComponent,
} from './modules/vaults/list/vaults-list.component';

import {
  VaultDetailsComponent,
} from './modules/vaults/details/vault-details.component';

import {
  BackupsListComponent,
} from './modules/backups/list/backups-list.component';

import {
  BackupDetailsComponent,
} from './modules/backups/details/backup-details.component';

import {
  SettingsComponent,
} from './modules/settings/settings.component';

// Auth components dependencies
import {
  SignInComponent
} from './app-core/auth/signin/signin.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'vaults-list',
    component: VaultsListComponent
  },
  {
    path: 'vault/:id',
    component: VaultDetailsComponent
  },
  {
    path: 'backups-list',
    component: BackupsListComponent
  },
  {
    path: 'backup/:id',
    component: BackupDetailsComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: 'signin',
    component: SignInComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
