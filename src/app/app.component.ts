// Angular dependencies
import {
  Component,
  OnInit,
} from '@angular/core';

// Service dependencies
import {
  OnlineService,
} from './service/online/service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss'
  ]
})

export class AppComponent implements OnInit {
  constructor(
    protected onlineService: OnlineService
  ) { }

  public async ngOnInit() {

    const infos = await this.onlineService.getUser();
    console.log(infos);
  }
}
