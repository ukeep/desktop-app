import {
  Component
} from '@angular/core';

export interface Section {
  name: string;
  updated: Date;
}

@Component({
  templateUrl: './vault-details.component.html',
  styleUrls: [
    './vault-details.component.scss'
  ],
})
export class VaultDetailsComponent {
  constructor() {}
}
