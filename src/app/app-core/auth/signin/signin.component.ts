import {
  Component,
  OnInit,
} from '@angular/core';

// Globals providers dependencies
import {
  OnlineGlobals
} from '../../../service/online/globals';

import {
  OvhGlobals
} from '../../../service/ovh/globals';

@Component({
  selector: 'app-provider-selection',
  templateUrl: './signin.component.html',
  styleUrls: [
    './signin.component.scss'
  ],
})

export class SignInComponent implements OnInit {
  /**
   * Stores the provider list
   */
  public $providers: any = [];

  /**
   * Stores the component title
   */
  public $componentTitle: string;

  constructor() { }

  public ngOnInit() {
    // Set the page title
    this.$componentTitle = 'Choose your provider';

    // Set the providers list
    this.$providers = [
      OnlineGlobals.GET_ONLINE,
      OvhGlobals.GET_OVH,
    ];
  }
}
