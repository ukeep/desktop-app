/**
 * Stores all the application globals variables
 */
export class Globals {
  /**
   * Return the Ukeep version
   */
  static get UKEEP_VERSION() {
    return '1.0';
  }
}
