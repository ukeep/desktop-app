// Angular dependencies
import {
  InjectionToken,
} from '@angular/core';

/**
 * Although the ApplicationConfig interface plays no role in dependency
 * injection, it supports typing of the configuration object within the class
 */
export interface ApplicationConfig {
  apiUrl: string;
  apiToken: string;
  sentryToken: string;
}

/**
 *  Configuration values for our app
 */
export const MY_CONFIG: ApplicationConfig = {
  apiUrl: 'http://localhost:8080/v1',
  apiToken: '',
  sentryToken: ''
};

/**
 * Create a config token to avoid naming conflicts
 */
export const MY_CONFIG_TOKEN = new InjectionToken<ApplicationConfig>('config');
