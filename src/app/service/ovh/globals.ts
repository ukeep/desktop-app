import {
  ovh,
} from '../../../../config';

/**
 * Stores OVH API configuration
 */
export class OvhGlobals {
  /**
   * Return OVH API configurations
   */
  static get CONF() {
    return {
      hostname: ovh.hostname,
      token: ovh.token,
      port: ovh.port,
      version: ovh.version,
    };
  }

  /**
   * Return OVH general informations
   */
  static get GET_OVH() {
    return {
      name: 'OVH',
      icon: './assets/pictures/ovh.png',
      description: ''
    };
  }
}
