import {
  HttpClient,
} from '@angular/common/http';

import {
  HttpHeaders,
} from '@angular/common/http';

import {
  Injectable,
} from '@angular/core';


import {
  Observable,
} from 'rxjs';

// Component  dependencies
import {
  ToolsApiService,
} from '../tools-api.service';


// import errorApi from './error';
@Injectable({
  providedIn: 'root'
})
export class Request {
  constructor(
    protected http: HttpClient
  ) { }

  public GET(options: any) {
    return new Promise<any>((resolve) => {
      this.http.get(options.path, options)
        .subscribe(data => resolve(data), err => console.log(err));
    });
  }

}
