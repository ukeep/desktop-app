// Angular dependencies
import {
  Injectable
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/**
 * Store tools needed for API exploitation
 */
export class ToolsApiService {

  constructor() { }

  /**
   * Function that converts to base64 the string parameter
   */
  public static baseConverter(str: string) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }
}
