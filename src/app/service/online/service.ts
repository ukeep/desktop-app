// Angular dependencies
import {
  Injectable,
} from '@angular/core';

import {
  HttpHeaders,
} from '@angular/common/http';

// Online globals dependencies
import {
  OnlineGlobals,
} from './globals';

// Import generic http requests functions
import {
  Request,
} from '../lib/request';

@Injectable({
  providedIn: 'root'
})

/**
 * API function sets call for Online provider
 */
export class OnlineService {
  constructor(
    protected request: Request,
  ) { }

  /**
   * Prepare Online headers call
   */
  private requestOptions(config: any, path = null) {
    // Store the real path
    const realPath = (typeof path === 'string') ? path : '';

    const header = {
      hostname: config.hostname,
      port: config.port,
      path: '/api/v' + config.version + realPath,
      path_prefix: '/api/v' + config.version,
      headers: new HttpHeaders({
        Authorization: `Bearer ${config.token}`,
        'Content-Type': 'application/json'
      })
    };

    return header;
  }

  /**
   * Return user token informations
   */
  public getUser() {
    // Define request options
    const options = {
      path: '/user',
      onFailureMessage: 'Unable to get current user infos.',
    };

    // Send the request
    return this.request.GET(this.requestOptions(OnlineGlobals.CONF, options.path));
  }

}
