import {
  online,
} from '../../../../config';

/**
 * Stores Online API configuration
 */
export class OnlineGlobals {
  /**
   * Return Online API configurations
   */
  static get CONF() {
    return {
      hostname: online.hostname,
      token: online.token,
      port: online.port,
      version: online.version,
    };
  }

  /**
   * Return Online general informations
   */
  static get GET_ONLINE() {
    return {
      name: 'Online',
      icon: './assets/pictures/online.png',
      description: ''
    };
  }
}
