// Angular dependencies
import {
  Injectable,
} from '@angular/core';

import {
  HttpClient,
} from '@angular/common/http';

import {
  HttpHeaders,
} from '@angular/common/http';

// Model dependencies
import {
  UserModel,
} from './user-model';

// Component  dependencies
import {
  ToolsApiService,
} from '../tools-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  protected apiURL: any = 'http://localhost:8080/v1';

  constructor(
    protected http: HttpClient,
  ) { }

  /**
   * Function that calls api route getUsers
   */
  public getUsers() {
    return new Promise(resolve => {

      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Basic ${ToolsApiService.baseConverter(`f54ffe310e77d69aa11eb98cf4972ba0939d6c96:`)}`,
        })
      };

      this.http.get<UserModel[]>(`${this.apiURL}/user`, httpOptions)
        .subscribe(data => resolve(data), err => console.log(err));
    });
  }
}
