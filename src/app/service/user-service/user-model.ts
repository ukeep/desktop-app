export class UserModel {
  id: number;
  login: string;
  email: string;
  firstName: string;
  lastName: string;
  company: string;
}
